from django.http.response import HttpResponseNotAllowed, HttpResponseNotFound, JsonResponse
from tascas.models import Tasca
from tascas.forms import TascaForm
from django.shortcuts import redirect, render
from django.http import HttpResponse, request
from django.db.models import Q

# Create your views here.
def home(request):
    # Render the HTML template index.html
    order = rating_order(request)
    if request.method == "GET":
        return render(request, 'index.html', {'form': TascaForm(), 'tascas': get_tascas("","",order), 'order': order})
    elif request.method == "POST":
        return render(request, 'index.html', {'form': TascaForm(), 'tascas': get_tascas(request.POST["query"],request.POST["query_type"],order), 'order': order})

def create(request):
    if request.method == 'POST':
        tasca_form = TascaForm(request.POST,request.FILES)
        if tasca_form.is_valid():
            tasca_form.save(commit=True)
        else:
            return render(request, 'index.html', {'form': TascaForm(), 'tascas': get_tascas("","","0"), 'erro': tasca_form.errors})
    else:
        return HttpResponseNotAllowed()
    
    return redirect('home')

def edit(request):
    if request.method == "POST":
        try:
            tasca_form = TascaForm(request.POST)
            if tasca_form.is_valid():
                tasca = Tasca.objects.filter(id=request.POST["id"]).first()
                if tasca is not None:
                    tasca.name = request.POST["name"]
                    tasca.address = request.POST["address"]
                    tasca.rating = request.POST["rating"]
                    tasca.map = request.POST["map"]
                    tasca.save()
                    return redirect('home')
            else:
                return render(request, 'index.html', {'form': TascaForm(), 'tascas': get_tascas("","","0"), 'erro': tasca_form.errors})
                    
        except KeyError as k:
            return JsonResponse({k.args[0]: "field missing in form"}, status=400)
    elif request.method == "GET":
        id = request.GET["id"]
        tasca = Tasca.objects.filter(id=id).first()
        tasca_form = TascaForm(instance=tasca)
        return render(request, 'edit.html', {'form': tasca_form, 'id': id})
        

def delete(request):
    if request.method == "POST":
        try:
            tasca = Tasca.objects.filter(id=request.POST["id"]).first()
            if tasca is not None:
                tasca.delete()
                return redirect('home')
            else:
                return HttpResponseNotFound()
        except KeyError as k:
            return JsonResponse({k.args[0]: "field missing in form"}, status=400)
    else:
        return HttpResponseNotAllowed()

def tasca_details(request):
    if request.method == "GET":
        try:
            tasca = Tasca.objects.filter(id=request.GET["id"]).first()
            if tasca is not None:
                return render(request,"details.html",{'tasca':tasca})
            else:
                return HttpResponseNotFound()
        except KeyError as k:
            return JsonResponse({k.args[0]: "field missing in form"}, status=400)
    else:
        return HttpResponseNotAllowed()

def rating_order(request):
    if request.method == "POST":
        try:
            order = request.POST["order"]
            if order == "0y":
                return "1"
            elif order == "1y":
                return "0"
            else:
                return "0"
        except KeyError as k:
            return "0"
    elif request.method == "GET":
        try:
            order = request.GET["order"]
            if order == "0y":
                return "1"
            elif order == "1y":
                return "0"
            else:
                return "0"
        except KeyError as k:
            return "0"
    else:
        return HttpResponseNotAllowed()
    

def get_tascas(query,query_type,order):
    if query == "":
        if order == "0": #1 to 10
            tascas = list(Tasca.objects.all().order_by('rating'))
        elif order == "1": #10 to 1
            tascas = list(Tasca.objects.all().order_by('-rating'))
        else:
            tascas = list(Tasca.objects.all())
        final_tascas = list((map(lambda x: x.serialize(),tascas)))
        return final_tascas
        
    else:
        tascas = [] #initialize
        if query_type == "1": #query by name
            if order == "0": #1 to 10
                tascas = list(Tasca.objects.filter(name__icontains=query).order_by('-rating'))
            elif order == "1": #10 to 1
                tascas = list(Tasca.objects.filter(name__icontains=query).order_by('rating'))
            else:
                tascas = list(Tasca.objects.filter(name__icontains=query))
        elif query_type == "2": #query by address
            if order == "0": #1 to 10
                tascas = list(Tasca.objects.filter(address__icontains=query).order_by('-rating'))
            elif order == "1": #10 to 1
                tascas = list(Tasca.objects.filter(address__icontains=query).order_by('rating'))
            else:
                tascas = list(Tasca.objects.filter(address__icontains=query))

        
        final_tascas = list((map(lambda x: x.serialize(),tascas)))
        return final_tascas