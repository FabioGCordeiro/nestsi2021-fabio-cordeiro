from django.forms import ModelForm, fields, forms
from tascas.models import Tasca
from django.core.files.images import get_image_dimensions

class TascaForm(ModelForm):
    error_messages = {
        'name_too_long':"The name exceeds the character limit.",
        'address_too_long':"The address exceeds the character limit.",
        'map_too_long':"The map exceeds the character limit.",
        'invalid_rating':"Please rate using numbers from 1 to 10.",
        'map_invalid_entry': "The map value inserted is not valid.",
    }
    class Meta:
        model = Tasca
        fields = ['name','address','rating','map']
        labels = {
            "map": "Map:",
            "rating": "Rating (1-10):"
            }
    
    def clean_name(self):
        name = self.cleaned_data.get('name')
        if len(name) > 50:
            raise forms.ValidationError(
                self.error_messages['name_too_long'],
                code='name_too_long',
            )
        return name
    
    def clean_address(self):
        address = self.cleaned_data.get('address')
        if len(address) > 50:
            raise forms.ValidationError(
                self.error_messages['address_too_long'],
                code='address_too_long',
            )
        return address
    
    def clean_map(self):
        map = self.cleaned_data.get('map')
        if len(map) > 100000:
            raise forms.ValidationError(
                self.error_messages['map_too_long'],
                code='map_too_long',
            )
        elif not map.startswith("https://"):
            if map != "none":
                raise forms.ValidationError(
                self.error_messages['map_invalid_entry'],
                code='map_invalid_entry',
            )
        return map

    def clean_rating(self):
        rating = self.cleaned_data.get('rating')
        if rating < 1 or rating > 10:
            raise forms.ValidationError(
                self.error_messages['invalid_rating'],
                code='invalid_rating',
            )
        return rating