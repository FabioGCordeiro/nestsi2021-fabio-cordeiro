from django.urls import path
from . import views
urlpatterns = [
    path('', views.home,name='home'),
    path('new', views.create,),
    path('edit', views.edit,),
    path('delete', views.delete,),
    path('tasca', views.tasca_details,),
]