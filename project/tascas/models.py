from django.db import models
import base64

def path_to_base64(path):
    file = open(path, "rb")
    data = base64.b64encode(file.read())
    file.close()
    return data.decode("utf-8")
# Create your models here.
class Tasca(models.Model):
    name = models.CharField(max_length=50, null=False)
    address = models.CharField(max_length=50, null=False)
    rating = models.IntegerField(null=False)
    map = models.CharField(max_length=100000,default="none",help_text="src link from google maps shared iframe.")

    def serialize(self):
        tasca = self
        return {
            "id": tasca.id,
            "name": tasca.name,
            "address": tasca.address,
            "rating": tasca.rating,
            "map": tasca.map,
        }