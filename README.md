NEST Summer Internship Challenge 2021
=====================================
Project proposal presented for NEST Collective's Summer Internship Challenge 2021.
Challenge instructions can be read at https://gitlab.com/simaonogueira/nestsi2020-technical-challenge-dev

Requirements
------------
### Install pip
```
# For linux users
$ sudo apt install python-pip
```
Checkout this link to install Pip on other platforms: [Install Pip](https://pip.pypa.io/en/stable/installing/)

### Install pipenv
```
# For linux users
$ sudo pip install pipenv
```
Checkout this link to install pipenv on other platforms: [Install Pipenv](https://pypi.org/project/pipenv/)

### Install PostgreSQL
```
# For linux users
$ sudo apt install postgresql postgresql-contrib
```
Checkout this link to install PostgreSQL on other platforms: [Install PostgreSQL](https://www.postgresql.org/download/)



Setup and Run
--------------------

1. Clone the repository
2. Create database named 'tascasdb'
```
# For linux users
$ sudo -i -u postgres
$ psql
$ ALTER USER postgres WITH PASSWORD 'password';
$ CREATE DATABASE tascasdb;
```
> You might want to use your username instead of 'postgres' in case you are already used to work with PostgreSQL.
More information on how to change psql's password, if needed: [Change Password](https://www.postgresql.org/docs/8.0/sql-alteruser.html)
3. Go to the repository
```
# For linux users
$ cd /nestsi2021-fabio-cordeiro/
```
4. Create virtual environment
```
# For linux users
$ sudo pipenv install
$ sudo pipenv sync
```
5. Change database credentials on nestsi2021-fabio-cordeiro/project/config/settings.py
> If you used the ALTER USER command in step 2 you don't need to make any changes to this file.
```
'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tascasdb',
        'USER': '*your username*',
        'PASSWORD': '*your password*',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
```

6. Access virtual environment
```
# For linux users
$ pipenv shell
```

8. Create database tables
```
# For linux users
$ cd /nestsi2021-fabio-cordeiro/project
$ python manage.py makemigrations
$ python manage.py migrate
```
9. Run the server
```
# For linux users
$ python manage.py runserver
```
10. Access the main page
> 127.0.0.1:8000/home/

Implemented Features
--------------------
- Add tascas;
- Edit tasca's information;
- Delete tascas;
- Click on the list to access tascas's details;
- Semi-validated forms: Adding and Editing a tasca with rating different than 1-10 produces an error message as well as inserting an invalid map link. (defined as semi-validated because it is only presented on the redirect to the home page, after the operation was considered invalid)
- New useful feature: Clicking on the rating header orders the table by rating;
- Search bar at the top with partial search by name or address, depending on the dropdown value;
- The list becomes scrollable when the number of entries reaches a point where it would overlap with the Add Tasca form.
- Show a Map with the location of each Tasca in the details page